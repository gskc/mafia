package com.gskc.mafia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class AddPlayers extends ActionBarActivity implements AdapterView.OnItemClickListener {

  private ArrayList<String> playerNames = new ArrayList<String>();
  private ArrayList<String> finalPlayerNames = null;
  private Map<String, Integer> voteCounts = new HashMap<String, Integer>();
  private Set<Integer> alreadyCastVotes = new HashSet<Integer>();

  private ArrayAdapter<String> arrayAdapter;
  private EditText addPlayerEditText;
  private ListView addedPlayerListView;
  private View addPlayerLinearLayout;
  private TextView headerTextView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_players);

    // Set up LinearLayout
    addPlayerLinearLayout = findViewById(R.id.addPlayerLinearLayout);
    addPlayerLinearLayout.setVisibility(View.VISIBLE);
    // Set up TextView
    headerTextView = (TextView) findViewById(R.id.headerTextView);
    headerTextView.setVisibility(View.INVISIBLE);

    // Set up ListView
    arrayAdapter = new ArrayAdapter<String>(
      this, R.layout.added_player, R.id.addedPlayerTextView, playerNames);
    addedPlayerListView = (ListView) findViewById(R.id.addedPlayerListView);
    addedPlayerListView.setAdapter(arrayAdapter);

    // Add default playerNames
    addPlayerEditText = (EditText) findViewById(R.id.addPlayerEditText);
    addPlayerEditText.setText("alpha");
    addPlayer(null);
    addPlayerEditText.setText("bravo");
    addPlayer(null);
    addPlayerEditText.setText("charlie");
    addPlayer(null);
    addPlayerEditText.setText("delta");
    addPlayer(null);
    addPlayerEditText.setText("echo");
    addPlayer(null);
    addPlayerEditText.setText("foxtrot");
    addPlayer(null);
    addPlayerEditText.setText("golf");
    addPlayer(null);
    addPlayerEditText.setText("hotel");
    addPlayer(null);
    addPlayerEditText.setText("india");
    addPlayer(null);
    addPlayerEditText.setText("juliet");
    addPlayer(null);

    startGame(null);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.add_players, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    return id == R.id.action_settings || super.onOptionsItemSelected(item);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (data == null || !data.hasExtra("lynch")) {
      return;
    }

    int voted = data.getIntExtra("lynch", -1);
    String votedPlayerName = finalPlayerNames.get(voted);
    int votes =
      voteCounts.containsKey(votedPlayerName)
        ? 1 + voteCounts.get(votedPlayerName)
        : 1;
    voteCounts.put(votedPlayerName, votes);
    alreadyCastVotes.add(requestCode);

    updateAfterVote(requestCode);
    updateAfterVote(voted);
    arrayAdapter.notifyDataSetChanged();
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    if (alreadyCastVotes.contains(position)) {
      Toast.makeText(this, finalPlayerNames.get(position) + " already voted!",
        Toast.LENGTH_SHORT).show();
      return;
    }

    startActivityForResult(
      new Intent(this, Citizen.class)
        .putExtra("player", finalPlayerNames.get(position))
        .putStringArrayListExtra("players", finalPlayerNames),
      position);
  }

  public void addPlayer(View view) {
    String playerName = addPlayerEditText.getText().toString();
    arrayAdapter.add(playerName);
  }

  public void startGame(View view) {
    if (finalPlayerNames == null) {
      finalPlayerNames = new ArrayList<String>(playerNames);
    }

    headerTextView.setVisibility(View.VISIBLE);
    headerTextView.setText("Cast your votes!");

    addPlayerLinearLayout.setVisibility(View.INVISIBLE);
    addedPlayerListView.setOnItemClickListener(this);
  }

  private void updateAfterVote(int playerId) {
    String playerName = finalPlayerNames.get(playerId);
    Integer votes = voteCounts.get(playerName);
    String message = playerName;
    if (alreadyCastVotes.contains(playerId)) {
      message += " has voted, ";
    }

    if (votes != null) {
      message += " has " + votes + " vote" + (votes > 1 ? "s" : "");
    }
    playerNames.set(playerId, message);
  }
}
